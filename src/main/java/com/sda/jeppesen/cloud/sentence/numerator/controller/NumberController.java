package com.sda.jeppesen.cloud.sentence.numerator.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;


@RestController
public class NumberController {

    //jeppesen.cloud.numbers = Trzy,Cztery,Pięć,Sześć
    @Value("${jeppesen.cloud.numbers}")
    private String[] numbers;

    @GetMapping("/number")
    public String getNumber(){
        // wylosuj liczbę z zakresu do ilości dostępnych wartości
        // zwróć słowo z podanego indeksu
        return numbers[new Random().nextInt(numbers.length)];
    }
}

// Mikroserwisy bankowe:
// - konta bankowe              // 100
// - lokaty                     // 5
// - notowania/giełdy/waluty    // 200