package com.sda.jeppesen.cloud.sentence.numerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NumeratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumeratorApplication.class, args);
	}

}
